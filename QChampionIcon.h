#ifndef QCHAMPIONICON_H
#define QCHAMPIONICON_H

#include <QWidget>
#include <QList>

namespace Ui {
class QChampionIcon;
}

class QChampionIcon : public QWidget
{
    Q_OBJECT
    
public:
    QChampionIcon(QWidget *parent = 0);
    QChampionIcon(QString ChampionName, QPixmap *ChampionIcon = 0, QWidget *parent = 0);
    ~QChampionIcon();

    void setPixmapFromFile(QString Filename);
    
private:
    Ui::QChampionIcon *ui;
};

#endif // QCHAMPIONICON_H
