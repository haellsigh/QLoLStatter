#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QPixmap>

namespace Ui {
class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void startRequest(QUrl url);
    QJsonDocument extractJsonDocument();

signals:
    void downloadResults(QByteArray data);

private slots:
    void downloadFile();
    void httpFinished();
    void httpReadyRead();
    void updateDataReadProgress(qint64 bytesRead, qint64 totalBytes);
    void getFreeChampions();
    void setFreeChampions(QByteArray data);
    
private:
    Ui::MainWindow *ui;
    QString apikey, baseurl;

    QUrl url;
    QNetworkAccessManager qnam;
    QNetworkReply *reply;
    int httpGetId;
    bool httpRequestAborted;
};

#endif // MAINWINDOW_H
