#include "QChampionIcon.h"
#include "ui_QChampionIcon.h"

QChampionIcon::QChampionIcon(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QChampionIcon)
{
    ui->setupUi(this);
}

QChampionIcon::QChampionIcon(QString ChampionName, QPixmap *ChampionIcon, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QChampionIcon)
{
    ui->setupUi(this);
    ui->ChampionName->setText(ChampionName);
    if(ChampionIcon != 0)
        ui->ChampionName->setPixmap(ChampionIcon);
}

void QChampionIcon::setPixmapFromFile(QString Filename)
{
    QPixmap IconFile(Filename);
    ui->ChampionIcon->setPixmap(IconFile);
}

QChampionIcon::~QChampionIcon()
{
    delete ui;
}
