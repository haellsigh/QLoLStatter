#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QtNetwork>

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow)
{
    apikey = "?api_key=270303ae-2637-4496-ad31-2b448541dafa";
    baseurl = "http://prod.api.pvp.net/";
    ui->setupUi(this);
    connect(ui->bFreeChamps, SIGNAL(clicked()), this, SLOT(getFreeChampions()));
    //connect(ui->bElse, SIGNAL(clicked()), this, SLOT())
    setWindowTitle(tr("HTTP"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::getFreeChampions()
{
    QUrl FreeChampionsUrl(baseurl + "api/lol/euw/v1.1/champion" + apikey + "&freeToPlay=true");

    ui->bFreeChamps->setEnabled(false);
    httpRequestAborted = false;

    reply = qnam.get(QNetworkRequest(FreeChampionsUrl));
    connect(reply, SIGNAL(finished()),
            this, SLOT(httpFinished()));
    connect(this, SIGNAL(downloadResults(QByteArray)), this, SLOT(setFreeChampions(QByteArray)));
    connect(reply, SIGNAL(downloadProgress(qint64,qint64)),
            this, SLOT(updateDataReadProgress(qint64,qint64)));
}

void MainWindow::setFreeChampions(QByteArray data)
{
    ui->bFreeChamps->setEnabled(true);
    QJsonDocument jsondocument = QJsonDocument::fromJson(data);
    if(!jsondocument.isObject())
        return;

    QJsonArray ChampionArray = jsondocument.object().take("champions").toArray();
    int ChampionCount = ChampionArray.count();
    int row = 0, col = 0;

    int i;
    QLabel *ChampionName;
    QPixmap ChampionIcon;
    QLabel *ChampionIconLabel;
    ChampionIconLabel = new QLabel;
    for(i = 0; i < ChampionCount; i++)
    {
        ChampionName = new QLabel(ChampionArray.at(i).toObject().value("name").toString());
        ChampionIcon = QPixmap("Images/Champions/Icons/" + ChampionArray.at(i).toObject().value("name").toString("NoName"));
        ChampionIconLabel = new QLabel;
        ChampionIconLabel->setPixmap(ChampionIcon);
        ui->layoutChamps->addWidget(ChampionName, row, col);
        //QMessageBox::information(this, tr("HTTP"), tr("Champion '%1' at x:%2 y:%3").arg(ChampionArray.at(i).toObject().value("name").toString()).arg(QString::number(i % 2)).arg(QString::number((int) floor((i % 5) / 2))));
        if(col == 5)
        {
            col = 0;
            row = 1;
        }
    }
}

void MainWindow::startRequest(QUrl url)
{
    reply = qnam.get(QNetworkRequest(url));
    connect(reply, SIGNAL(finished()),
            this, SLOT(httpFinished()));
    //connect(reply, SIGNAL(readyRead()),
    //        this, SLOT(httpReadyRead()));
    connect(reply, SIGNAL(downloadProgress(qint64,qint64)),
            this, SLOT(updateDataReadProgress(qint64,qint64)));
}

void MainWindow::downloadFile()
{
    url = ui->eURL->text();

    setWindowTitle(tr("HTTP"));
    ui->status_Label->setText(tr("Downloading ..."));

    // schedule the request
    httpRequestAborted = false;
    startRequest(url);
}

void MainWindow::httpFinished()
{
    if (httpRequestAborted)
    {
        reply->deleteLater();
        return;
    }

    if (reply->error())
    {
        QMessageBox::information(this, tr("HTTP"),
                                 tr("Download failed: %1.")
                                 .arg(reply->errorString()));
    }
    //ui->eOutput->setText(reply->readAll());
    emit downloadResults(reply->readAll());
    ui->status_Label->setText(tr("Download completed."));
    reply->deleteLater();
    reply = 0;
}

void MainWindow::httpReadyRead()
{
    // ???? Nothing for now
}

void MainWindow::updateDataReadProgress(qint64 bytesRead, qint64 totalBytes)
{
    if (httpRequestAborted)
        return;

    ui->status_Progress->setMaximum(totalBytes);
    ui->status_Progress->setValue(bytesRead);
}
